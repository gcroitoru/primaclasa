package bicicleta;

public class Bicicleta {
    String model;
    private int greutate;
    private int viteza;
    private static int nrBiciclete = 0;
    public Bicicleta(String model, int gr, int viteza){
        this.model = model;
        this.greutate = gr;
        this.viteza = viteza;
        nrBiciclete++;
    }
    public Bicicleta(String model, int greutate){
        this.model = model;
        this.greutate = greutate;
        nrBiciclete++;
    }
    static int getNrBiciclete(){
        return nrBiciclete;
    }
    public int getViteza(){
        return viteza;
    }
    public void setViteza(int viteza){
        this.viteza = viteza;
    }
    public int getGreutate(){
        return this.greutate;
    }
    public void comparareViteza(Bicicleta bic){
        if(bic.viteza > viteza){
            System.out.println("bicicleata " + bic.model + " are viteza mai mare!");
        }else{
            System.out.println("bicicleata " + model + " are viteza mai mare!");
        }

    }
    public static void comparareViteza(Bicicleta a1, Bicicleta a2){
        if (a1.viteza > a2.viteza){
            System.out.println("bicicleata " + a1.model + " are viteza mai mare!");
        }else{
            System.out.println("bicicleata " + a2.model + " are viteza mai mare!");
        }
    }
}
