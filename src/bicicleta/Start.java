package bicicleta;

import java.util.Random;

public class Start {
    public static void main(String[] args) {
        Bicicleta b1 = new Bicicleta("Cube", 15, 20);
        Bicicleta b2 = new Bicicleta("Merida", 12);
        Random rand = new Random();

        System.out.printf("b1: model = %s greutate = %d viteza = %d nrBiciclete = %d\n", b1.model, b1.getGreutate(), b1.getViteza(), Bicicleta.getNrBiciclete());
        System.out.printf("b2: model = %s greutate = %d viteza = %d nrBiciclete = %d\n", b2.model, b2.getGreutate(), b2.getViteza(), Bicicleta.getNrBiciclete());
        b2.setViteza(rand.nextInt(21)+10);
        System.out.printf("b2: model = %s greutate = %d viteza = %d nrBiciclete = %d\n", b2.model, b2.getGreutate(), b2.getViteza(), Bicicleta.getNrBiciclete());
        if (b1.getViteza() > b2.getViteza()){
            System.out.println("bicicleata " + b1.model + " are viteza mai mare!");
        }else{
            System.out.println("bicicleata " + b2.model + " are viteza mai mare!");
        }

        Bicicleta.comparareViteza(b1, b2); // comparare folosind metoda statica
        b1.comparareViteza(b2); // comparare folosind metoda de instanta a obiectului b1
        b2.comparareViteza(b1);
    }
}
