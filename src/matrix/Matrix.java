package matrix;

import java.util.Random;

public class Matrix {
    protected int[][] matrice = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    private int nrLinii = 3;
    private int nrColoane = 3;

    public Matrix() {
    }

    public Matrix(int linii, int coloane) {
        nrLinii = linii;
        nrColoane = coloane;
        matrice = new int[linii][coloane];
        Random rand = new Random();
        for (int i = 0; i < linii; i++) {
            for (int j = 0; j < coloane; j++) {
                matrice[i][j] = 10 + rand.nextInt(90);
            }
        }
    }

    public void afisareMatrice() {
        System.out.println();
        for (int linie = 0; linie < matrice.length; linie++) {
            for (int coloana = 0; coloana < matrice[linie].length; coloana++) {
                System.out.print(matrice[linie][coloana] + " ");
            }
            System.out.println();
        }
    }

    public void modificareLinie(int linie, int valoare) {
        for (int coloana = 0; coloana < matrice[linie].length; coloana++) {
            matrice[linie][coloana] = valoare;
        }
        System.out.println();
    }

    public static boolean copiereMatrix(Matrix dest, Matrix src) {
        if ((dest.nrLinii == src.nrLinii) && (dest.nrColoane == src.nrColoane)) {
            for (int i = 0; i < dest.nrLinii; i++) {
                for (int j = 0; j < dest.nrColoane; j++) {
                    dest.matrice[i][j] = src.matrice[i][j];
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean copiereMatrix(Matrix src) {
        if ((nrLinii == src.nrLinii) && (nrColoane == src.nrColoane)) {
            for (int i = 0; i < nrLinii; i++) {
                for (int j = 0; j < nrColoane; j++) {
                    matrice[i][j] = src.matrice[i][j];
                }
            }
            return true;
        } else {
            return false;
        }
    }
    public Matrix clonareMatrix(){
        Matrix clona = new Matrix(nrLinii,nrColoane);
        for (int i = 0; i < nrLinii; i++) {
            for (int j = 0; j < nrColoane; j++) {
                clona.matrice[i][j] = matrice[i][j];
            }
        }
        return clona;
    }

    public static void bubleSortCrescator(int[] sir){
        for (int i = 0; i < sir.length; i++){
            boolean amSchimbat = false;
            for (int j = 1; j < (sir.length - i); j++){
                int temp;
                if (sir[j-1] > sir[j]){
                    temp =  sir[j];
                    sir[j] = sir[j-1];
                    sir[j-1] = temp;
                    amSchimbat = true;
                }
            }
            if (amSchimbat ==  false){
                break;
            }
        }
    }
}
