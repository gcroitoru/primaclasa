package matrix;

import java.util.Arrays;

public class StartMatrix {
    public static void main(String[] args) {
    Matrix m1 = new Matrix();

    System.out.println("Matricea m1 e:");
    m1.afisareMatrice();
    int valoare = 5;
    m1.modificareLinie(2, valoare);
    m1.afisareMatrice();

    Matrix m2 = new Matrix(4,5);
    System.out.print("\nMatricea m2 e:");
    m2.afisareMatrice();
    Matrix.bubleSortCrescator(m2.matrice[0]);

    Arrays.sort(m2.matrice[1]);
    Arrays.fill(m2.matrice[2], 55);
    m2.matrice[3] = Arrays.copyOf(m2.matrice[0], m2.matrice[0].length);
    System.out.print("\nMatricea m2 dupa sortare linie 0 e:");
    m2.afisareMatrice();

    Matrix m3 = new Matrix(4,5);
    if (Matrix.copiereMatrix(m3, m2) == false){ // folosim copierea m2 in m3 cu metoda statica
        System.out.println("Matricile nu au aceeasi dimensiune!");
    }
    System.out.print("\nMatricea m3 e:");
    m3.afisareMatrice();

    Matrix m4 = new Matrix(4,5);
    if (m4.copiereMatrix(m2) == false){ // folosim copierea m2 in m4 cu metoda non statica
       System.out.println("Matricile nu au aceeasi dimensiune!");
   }
    System.out.print("\nMatricea m4 e:");
    m4.afisareMatrice();

    Matrix m5;
    m5 = m2.clonareMatrix(); // copiem m2 in m5 folosind metoda de clonare ce ne intoarce adresa obiectului clona
    System.out.print("\nMatricea m5 e:");
    m5.afisareMatrice();

    // In momentul asta avem m3, m4 si m5, copii ale lui m2 obtinute prin 3 metode diferite
    System.out.println("Urmeaza sa modificam m3:");
    m3.modificareLinie(0, 1); // aici modificam m3, care e o copie a lui m2. m2 ramane neschimbat

    System.out.print("\nMatricea m2 e:");
    m2.afisareMatrice();
    System.out.print("\nMatricea m3 e:");
    m3.afisareMatrice();
    }
}
