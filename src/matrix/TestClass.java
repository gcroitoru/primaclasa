package matrix;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Objects;

public class TestClass {
    int varsta;
    String nume;
    String prenume;

    public TestClass(int varsta, String nume, String prenume) {
        this.varsta = varsta;
        this.nume = nume;
        this.prenume = prenume;
    }

    public TestClass(String nume, String prenume) {
        this.nume = nume;
        this.prenume = prenume;
    }

    public int getVarsta() {
        return varsta;
    }

    public void setVarsta(int varsta) {
        this.varsta = varsta;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestClass testClass = (TestClass) o;
        return varsta == testClass.varsta &&
                nume.equals(testClass.nume) &&
                prenume.equals(testClass.prenume);
    }

    @Override
    public int hashCode() {
        return Objects.hash(varsta, nume, prenume);
    }

    //@Override

    public String toString() {
        return "Obiectule: {" +
                "varsta=" + varsta +
                ", nume='" + nume + '\'' +
                ", prenume='" + prenume + '\'' +
                '}';
    }
}

 class TestClassMain {
    public static void testParametrii(int a, int[] b){
        a = 10;
        b[0] = 111;
        b[1] = 222;
    }
     public static void main(String[] args) {
         System.out.println("Parametrii primiti sunt:");
         for(String x : args){
             System.out.println(x + " ");
         }
         int y = 1;
         int[] z = {1,1};
         testParametrii(y, z);
         System.out.println("y: " + y + " z: " + Arrays.toString(z));

         int var1 = 7;
         Integer var2;
         Integer var3 = new Integer(7);
         var1 = Integer.parseInt("123");
        // Integer.tob
         System.out.println("var1=" + var1 +  " iar in baza 2 e:" + Integer.toBinaryString(var1));

         TestClass a = new TestClass(22, "Popescu", "Mihai");
         TestClass b = new TestClass(22, "Popescu", "Mihai");
         TestClass c = new TestClass(42, "Popescu", "Mihai");
         System.out.println("a == b : " + a.equals(b));
         System.out.println("a == c : " + a.equals(c));
         System.out.println("getClass:" + a.getClass());
         System.out.println("hashCode a:" + a.hashCode());
         System.out.println("hashCode b:" + b.hashCode());
         System.out.println("hashCode c:" + c.hashCode());
         System.out.println("a.toString: " + a.toString());
     }
 }


