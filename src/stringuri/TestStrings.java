package stringuri;

public class TestStrings {
    public static void main(String[] args) {
        int nrRulari = 10000;
        long x = stressStringTest(nrRulari);
        System.out.println("String: " + x + " milisec");

        long y = stressBuilderTest(nrRulari);
        System.out.println("String Builder: " + y + " milisec");
    }

    public static long stressStringTest(int n) {

        String r = new String("");
        String a = new String("Hello world !");
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            r += a;
        }
        long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    public static long stressBuilderTest(int n) {
        StringBuilder r = new StringBuilder("");
        StringBuilder a = new StringBuilder("Hello world !");

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            r = r.append(a);
        }
        long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }
}
